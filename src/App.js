import React from 'react';
import './App.css';

function App() {
  let chat=[];

  function write(){
  chat.push(document.getElementById("chat").value)   
  localStorage.setItem("chat",JSON.stringify(chat))
  };

  const handleShowSubmit = (element) => {
    console.log(JSON.parse(window.localStorage.getItem('chat')))
  }

   
  return (
      <div className="App">
      <div className="chatArea"><input type="text" id="chat"></input>
      <br></br>
          <button onClick={write}>Send</button>
          <br></br>
          <button onClick={handleShowSubmit}>Show messages</button>
          </div>
      <p>
      Lorem ipsum dolor sit amet, consectetur adipiscing elit, 
      sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. 
      Iaculis eu non diam phasellus vestibulum lorem sed risus. 
      Sed faucibus turpis in eu mi bibendum neque egestas. 
      Nibh praesent tristique magna sit. 
      Nec sagittis aliquam malesuada bibendum arcu vitae elementum curabitur vitae. 
      Massa massa ultricies mi quis hendrerit dolor magna eget. 
      At elementum eu facilisis sed odio morbi quis. 
      Hac habitasse platea dictumst quisque sagittis purus sit amet volutpat. 
      Mauris commodo quis imperdiet massa tincidunt.
      Pellentesque sit amet porttitor eget dolor morbi non arcu. 
      Vitae suscipit tellus mauris a diam maecenas. 
      Sed odio morbi quis commodo odio aenean. Vitae congue eu consequat ac felis. 
      Non sodales neque sodales ut etiam. Lobortis mattis aliquam faucibus purus in massa tempor. 
      Et magnis dis parturient montes nascetur ridiculus mus mauris vitae.
       Aliquam sem et tortor consequat id porta. In nibh mauris cursus mattis molestie a.
      </p>
      <p>
      Morbi tincidunt augue interdum velit euismod in. 
      Pulvinar mattis nunc sed blandit. Tristique risus nec feugiat in. 
      Pellentesque eu tincidunt tortor aliquam nulla facilisi cras fermentum.
       Ornare massa eget egestas purus viverra accumsan in nisl nisi. 
       Aliquam sem et tortor consequat id porta. Neque ornare aenean euismod elementum nisi quis eleifend. 
       Massa tincidunt nunc pulvinar sapien et ligula ullamcorper malesuada proin.
       Donec pretium vulputate sapien nec sagittis aliquam malesuada. Nunc sed velit dignissim sodales.
      </p>
      <p>
      Aliquam sem et tortor consequat id porta nibh venenatis cras. 
      Odio euismod lacinia at quis risus sed vulputate odio. 
      Volutpat commodo sed egestas egestas. Curabitur gravida arcu ac tortor. 
      Neque aliquam vestibulum morbi blandit cursus risus. Aliquam malesuada bibendum arcu vitae. 
      Elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi. 
      Urna nec tincidunt praesent semper. Ut sem nulla pharetra diam sit amet. 
      Ante in nibh mauris cursus mattis molestie a iaculis.
      Aliquam sem et tortor consequat id porta nibh venenatis cras. 
      Odio euismod lacinia at quis risus sed vulputate odio. 
      Volutpat commodo sed egestas egestas. Curabitur gravida arcu ac tortor. 
      Neque aliquam vestibulum morbi blandit cursus risus. Aliquam malesuada bibendum arcu vitae. 
      Elementum facilisis leo vel fringilla est ullamcorper eget nulla facilisi. 
      Urna nec tincidunt praesent semper. Ut sem nulla pharetra diam sit amet. 
      Ante in nibh mauris cursus mattis molestie a iaculis.
      </p>
      <p>
      Sit amet justo donec enim. Lorem dolor sed viverra ipsum nunc. 
      Senectus et netus et malesuada. Fringilla est ullamcorper eget nulla. 
      Aliquet nec ullamcorper sit amet risus nullam. 
      Est ullamcorper eget nulla facilisi etiam dignissim diam quis enim. 
      Orci phasellus egestas tellus rutrum tellus. Volutpat diam ut venenatis tellus in metus vulputate eu. 
      Malesuada fames ac turpis egestas maecenas pharetra convallis. 
      Nam at lectus urna duis convallis convallis tellus id interdum.
      </p>
      <p>
      Vitae sapien pellentesque habitant morbi. Tristique senectus et netus et malesuada fames ac. 
      Malesuada bibendum arcu vitae elementum curabitur vitae nunc sed velit.
       Arcu dui vivamus arcu felis bibendum ut tristique et. Congue mauris rhoncus aenean vel. 
       Phasellus vestibulum lorem sed risus ultricies. 
       Odio eu feugiat pretium nibh ipsum consequat nisl. Id faucibus nisl tincidunt eget nullam non. 
       Ultricies integer quis auctor elit sed vulputate mi sit. 
       Non pulvinar neque laoreet suspendisse. Nisi vitae suscipit tellus mauris a diam.
      </p>
    </div>
    

  );
}

export default App;
